'use strict';

module.exports = function(sequelize, DataTypes) {
	var My_Model__c = sequelize.define('My_Model__c', {
		id: DataTypes.INTEGER,
		sfid: DataTypes.TEXT,
		isdeleted: DataTypes.BOOLEAN,
		name: DataTypes.TEXT,
		title__c: DataTypes.TEXT,
		model_description__c: DataTypes.TEXT,
		systemmodstamp: DataTypes.DATE,
		_hc_err: DataTypes.TEXT,
		_hc_lastop: DataTypes.TEXT
	}, {
		schema: 'salesforce',
		freezeTableName: true
	});

	return My_Model__c;
};




