'use strict';

var pg = require('pg');

/**
 * List of Service Orders
 */
exports.list = function(request, response) {
	console.log(process.env.NODE_ENV);
	if (process.env.NODE_ENV !== 'development') {
		console.log('not on local dev.');
		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
			client.query('SELECT * FROM salesforce.Service_Order__c', function(err, result) {
				done();

				if (err) {
					console.error(err);
					response.json('Error ' + err);
				} else {
					console.log('service order data!');
					response.json(result.rows);
				}
			});
		});
	} else {
		console.log('on local dev.');
	}

};


/**
 * Create a Service Order
 */
exports.create = function(request, response) {
	console.log('create service order record');
	console.log(process.env.NODE_ENV);
	if (process.env.NODE_ENV !== 'development') {
		console.log('not on local dev.');		
		
		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
			// create record
			client.query('INSERT INTO salesforce.Service_Order__c(name, account__c, order_type__c, term__c, order_stage__c) values($1,$2,$3,$4,$5)', ['Service Order Test 0002', '001g000000bYJ6bAAG', 'Renewal', 1, 'Draft'], function(err, result) {
				done();

				if (err) {
					console.error(err);
					response.json('Error ' + err);
				} else {
					console.log('inserted row');
				}
			});


		});

	} else {
		console.log('on local dev.');
	}

};