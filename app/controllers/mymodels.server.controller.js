'use strict';

var models  = require('../../config/sequelize');

/**
 * List of My Models
 */
exports.list = function(request, response) {
	if (process.env.NODE_ENV !== 'development') {
		models.My_Model__c.findAll().then(function(result) {
			response.json(result);
		});

	} else {
		console.log('on local dev.');
	}

};