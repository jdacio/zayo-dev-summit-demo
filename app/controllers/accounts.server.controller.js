'use strict';

var pg = require('pg');

/**
 * List of Accounts
 */
exports.list = function(request, response) {
	console.log('list accounts');
	console.log(process.env.NODE_ENV);
	if (process.env.NODE_ENV !== 'development') {
		console.log('not on local dev.');
		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
			client.query('SELECT * FROM salesforce.account', function(err, result) {
				done();

				if (err) {
					console.error(err);
					response.json('Error ' + err);
				} else {
					console.log('acct data!');
					response.json(result.rows);
				}
			});
		});
	} else {
		console.log('on local dev.');
	}

};

/**
 * Create an Account
 */
exports.create = function(request, response) {
	console.log('create account record');
	console.log(process.env.NODE_ENV);
	if (process.env.NODE_ENV !== 'development') {
		console.log('not on local dev.');		
		
		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
			// create record
			client.query('INSERT INTO salesforce.account(name) values($1)', ['Justice League of America'], function(err, result) {
				done();

				if (err) {
					console.error(err);
					response.json('Error ' + err);
				} else {
					console.log('inserted row');
				}
			});


		});

	} else {
		console.log('on local dev.');
	}

};