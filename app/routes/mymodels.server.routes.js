'use strict';

/**
 * Module dependencies.
 */
var mymodels = require('../../app/controllers/mymodels.server.controller');

module.exports = function(app) {
	// Account Routes
	app.route('/mymodels')
		.get(mymodels.list);
};
