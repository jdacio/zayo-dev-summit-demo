'use strict';

/**
 * Module dependencies.
 */
var serviceorders = require('../../app/controllers/serviceorders.server.controller');

module.exports = function(app) {
	// Account Routes
	app.route('/serviceorders')
		.get(serviceorders.list)
		.post(serviceorders.create);
};
