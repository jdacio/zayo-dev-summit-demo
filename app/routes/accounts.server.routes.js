'use strict';

/**
 * Module dependencies.
 */
var accounts = require('../../app/controllers/accounts.server.controller');

module.exports = function(app) {
	// Account Routes
	app.route('/accounts')
		.get(accounts.list)
		.post(accounts.create);
};
