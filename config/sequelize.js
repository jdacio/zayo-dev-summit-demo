'use strict';
var db = {};

var Sequelize = require('sequelize'),
		path = require('path'),
		config = require('./config');

var sequelize = null;

if (process.env.DATABASE_URL != null) {
		sequelize = new Sequelize(process.env.DATABASE_URL, {
			quoteIdentifiers: false,
			define: {
				timestamps: false
			},
			logging: console.log,
		});
}

config.getGlobbedFiles('./app/models/postgres/**/*.js').forEach(function(modelPath) {
		var model = sequelize.import(path.resolve(modelPath));
		db[model.name] = model;
});

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db;