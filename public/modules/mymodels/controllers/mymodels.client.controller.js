'use strict';

// Accounts controller
angular.module('mymodels').controller('MyModelsController', [
	'$scope', '$http', 'httpq', function($scope, $http, httpq) {
		httpq.get('/mymodels')
			.then(function(data) {
				$scope.myModels = data;
			})
			.catch(function(response) {
				console.error('my models retrieval error', response.status, response.data);
			})
			.finally(function() {
				console.log('yippee! got my models.');
			});
	}
]);