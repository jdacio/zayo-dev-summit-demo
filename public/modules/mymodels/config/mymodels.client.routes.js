'use strict';

// Setting up route
angular.module('mymodels').config(['$stateProvider',
	function($stateProvider) {
		// Accounts state routing
		$stateProvider.
		state('mymodels', {
			url: '/mymodels',
			views: {
				main: {
					templateUrl: 'modules/mymodels/views/list-mymodels.client.view.html',
					controller: 'MyModelsController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).
		state('mymodels.list', {
			url: '/list',
		});
	}
]);