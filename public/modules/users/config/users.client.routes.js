'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('users', {
			url: '/users',
			views: {
				main: {
					templateUrl: 'modules/users/views/authentication/signup.client.view.html',
					controller: 'AuthenticationController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).

		state('users.profile', {
			url: '^/settings/profile',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/settings/edit-profile.client.view.html',
					controller: 'SettingsController'
				}
			}
		}).
		state('users.password', {
			url: '^/settings/password',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/settings/change-password.client.view.html',
					controller: 'SettingsController'
				}
			}
		}).
		state('users.accounts', {
			url: '^/settings/accounts',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/settings/social-accounts.client.view.html',
					controller: 'SettingsController'
				}
			}
		}).
		state('users.signup', {
			url: '^/signup',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/authentication/signup.client.view.html',
					controller: 'AuthenticationController'
				}
			}
		}).
		state('users.signin', {
			url: '^/signin',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/authentication/signin.client.view.html',
					controller: 'AuthenticationController'
				}
			}
		}).
		state('users.forgot', {
			url: '^/password/forgot',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/password/forgot-password.client.view.html',
					controller: 'PasswordController'
				}
			}
		}).
		state('users.reset-invalid', {
			url: '^/password/reset/invalid',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
				}
			}
		}).
		state('users.reset-success', {
			url: '^/password/reset/success',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
				}
			}
		}).
		state('users.reset', {
			url: '^/password/reset/:token',
			views: {
				'main@': {
					templateUrl: 'modules/users/views/password/reset-password.client.view.html',
					controller: 'PasswordController'
				}
			}
		});
	}
]);