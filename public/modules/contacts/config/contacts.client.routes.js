'use strict';

// Setting up route
angular.module('contacts').config(['$stateProvider',
	function($stateProvider) {
		// Contacts state routing
		$stateProvider.
		state('contacts', {
			url: '/contacts',
			views: {
				main: {
					templateUrl: 'modules/contacts/views/list-contacts.client.view.html',
					controller: 'ContactsController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).
		state('contacts.list', {
			url: '/list',
		});
	}
]);