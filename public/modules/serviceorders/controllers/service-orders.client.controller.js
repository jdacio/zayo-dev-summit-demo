'use strict';

// Accounts controller
angular.module('serviceorders').controller('ServiceOrderController', [
	'$scope', '$http', 'httpq', function($scope, $http, httpq) {
		$scope.createSvcOrder = function() {
			console.log('create button');
			$http.post('/serviceorders');
		};


		httpq.get('/serviceorders')
			.then(function(data) {
				console.log(data);
				$scope.serviceOrders = data;
			})
			.catch(function(response) {
				console.error('service order retrieval error', response.status, response.data);
			})
			.finally(function() {
				console.log('yippee! got service orders.');
			});
	}
]);