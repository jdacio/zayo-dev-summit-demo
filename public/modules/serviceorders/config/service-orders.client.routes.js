'use strict';

// Setting up route
angular.module('serviceorders').config(['$stateProvider',
	function($stateProvider) {
		// Accounts state routing
		$stateProvider.
		state('serviceorders', {
			url: '/service-orders',
			views: {
				main: {
					templateUrl: 'modules/serviceorders/views/list-service-orders.client.view.html',
					controller: 'ServiceOrderController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).
		state('serviceorders.list', {
			url: '/list',
		});
	}
]);