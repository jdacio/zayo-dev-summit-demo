'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('articles', {
			url: '/articles',
			views: {
				main: {
					templateUrl: 'modules/articles/views/list-articles.client.view.html',
					controller: 'ArticlesController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).
		state('articles.list', {
			url: '/list',
		}).
		state('articles.create', {
			url: '/create',
			views: {
				'main@': {
					templateUrl: 'modules/articles/views/create-article.client.view.html',
					controller: 'ArticlesController'
				}
			}
		}).
		state('articles.view', {
			url: '/:articleId',
			views: {
				'main@': {
					templateUrl: 'modules/articles/views/view-article.client.view.html',
					controller: 'ArticlesController'
				}
			}
		}).
		state('articles.edit', {
			url: '/:articleId/edit',
			views: {
				'main@': {
					templateUrl: 'modules/articles/views/edit-article.client.view.html',
					controller: 'ArticlesController'
				}
			}
		});
	}
]);