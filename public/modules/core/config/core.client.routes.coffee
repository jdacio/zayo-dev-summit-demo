"use strict"

# Setting up route
angular.module("core").config [
	"$stateProvider", "$urlRouterProvider", ($stateProvider, $urlRouterProvider) ->
	
		# Redirect to home view when route not found
		$urlRouterProvider.otherwise "/"
		
		# Home state routing
		$stateProvider
		.state "home",
			url: "/"
			views:
				main:
					templateUrl: "modules/core/views/home.client.view.html"
					controller: "HomeController"

				header:
					templateUrl: "modules/core/views/header.client.view.html"
					controller: "HeaderController"

				footer:
					templateUrl: "modules/core/views/footer.client.view.html"
					controller: "FooterController"

		.state "home.kitchensink",
			url: "kitchensink"
			views:
				"main@":
					templateUrl: "modules/core/views/kitchensink.client.view.html"

		.state "home.flexbox",
			url: "flexbox"
			views:
				"main@":
					templateUrl: "modules/core/views/flexbox.client.view.html"

		.state "home.layout",
			url: "layout"
			views:
				"main@":
					templateUrl: "modules/core/views/layout.client.view.html"

		.state "home.layout.test",
			url: "/test"
			views:
				left:
					templateUrl: "modules/core/views/left.client.view.html"
					controller: "LeftController"

				middle:
					templateUrl: "modules/core/views/middle.client.view.html"

				right:
					templateUrl: "modules/core/views/right.client.view.html"

]