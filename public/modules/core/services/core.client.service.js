'use strict';
angular.module('core')
	.factory('httpq', [
		'$http', '$q', function($http, $q) {
			var httpq;
			httpq = {};

			httpq.get = function() {
				var deferred = $q.defer();

				$http.get.apply(null, arguments)
					.success(deferred.resolve)
					.error(deferred.resolve);

				return deferred.promise;
			};

			return httpq;
		}
	]);