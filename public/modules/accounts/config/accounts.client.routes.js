'use strict';

// Setting up route
angular.module('accounts').config(['$stateProvider',
	function($stateProvider) {
		// Accounts state routing
		$stateProvider.
		state('accounts', {
			url: '/accounts',
			views: {
				main: {
					templateUrl: 'modules/accounts/views/list-accounts.client.view.html',
					controller: 'AccountsController'
				},
				header: {
					templateUrl: 'modules/core/views/header.client.view.html',
					controller: 'HeaderController'
				},
				footer: {
					templateUrl: 'modules/core/views/footer.client.view.html',
					controller: 'FooterController'
				}
			}
		}).
		state('accounts.list', {
			url: '/list',
		});
	}
]);