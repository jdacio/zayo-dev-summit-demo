'use strict';

// Accounts controller
angular.module('accounts').controller('AccountsController', [
	'$scope', '$http', 'httpq', function($scope, $http, httpq) {
		$scope.createAcct = function() {
			console.log('create button');
			$http.post('/accounts');
		};

		httpq.get('/accounts')
			.then(function(data) {
				$scope.accounts = data;
			})
			.catch(function(response) {
				console.error('account retrieval error', response.status, response.data);
			})
			.finally(function() {
				console.log('yippee! got accounts.');
			});
	}
]);