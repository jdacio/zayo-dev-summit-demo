'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName)
	.run([
		"$rootScope", "$state", "$stateParams", "$location", function($rootScope, $state, $stateParams, location, $oauth2) {
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;
			return $rootScope.$on("$stateChangeSuccess", function() {
				window.scrollTo(0, 0);
			});
		}
	])
	.config(['$locationProvider',
		function($locationProvider) {
			$locationProvider.hashPrefix('!');
		}
	]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});